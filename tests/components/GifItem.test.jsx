import { render, screen } from "@testing-library/react";
import { GifItem } from "../../src/components"

describe('Tests in <GifItem/>', () => {
    const title = 'Pascua GIF by Alanika';
    const url = 'https://media3.giphy.com/media/dxZhRaYzH00M4D4Pn7/giphy.gif?cid=6202133edasn0uvx9dcbqnfrbs8q8qe6tzcwc6o78u7nroaf&ep=v1_gifs_search&rid=giphy.gif&ct=g';

    test('Have to do match with the snapshot', () => {
        const { container } = render(<GifItem title= { title } url= { url }/>);
        expect(container).toMatchSnapshot();
    })

    test('Have to show the image with the URL and the ALT', () => {
        render(<GifItem title = { title } url ={ url } />);
        const { alt, src }  = screen.getByRole('img'); 
        expect( src ).toBe(url);
        expect( alt ).toBe(title);
    })

    test('have to show the title in the component', () => {
        render(<GifItem title = { title } url = { url } />);
        expect ( screen.findByText(title) ).toBeTruthy();
    })
})

