import { getGifs } from "../../src/helpers/getGifs"

describe('Tests in the getGifs file', () => {
   
    const category = 'One Punch';

    test('have to return an array of gifs', async() => {
        const gifs  =  await getGifs(category);
        expect(gifs.length).toBeGreaterThan(0);
        expect(gifs[0]).toEqual({
            id: expect.any( String ),
            title: expect.any( String ), 
            url: expect.any( String ),
        })
    })
})