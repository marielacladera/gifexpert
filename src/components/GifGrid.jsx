import { useFetchGifs } from "../hooks/useFetchGifs";
import { GifItem } from './GifItem';
import PropTypes from 'prop-types';

export const GifGrid = ({ category, deleteCategory }) => {

    const { images, isLoading } = useFetchGifs(category);

    // const [images, setImages] = useState([])

    // const getImages = async() => {
    //     const newImages = await getGifs(category);
    //     setImages(newImages);
    // }

    // useEffect(() => {
    //     // getGifs(category).then( newImages => 
    //     //     setImages(newImages)
    //     // );
    //     getImages();
    // }, []);

    const onDelete = () => {
        deleteCategory(category);
    }

  return (
    <>
        <div className="category">
            <h3>{ category }</h3>
            <button onClick={ onDelete }>Delete Category</button>
        </div>
        {
            isLoading && (<h2>Cargando...</h2>)
        }

        <div className="card-grid">
            { images.map(( image ) => 
                (<GifItem
                    key = { image.id }
                    { ...image }
                />)
            )}
        </div>
    </>
  )
}

GifGrid.propTypes = {
    category: PropTypes.string.isRequired,
    deleteCategory : PropTypes.func.isRequired
}
