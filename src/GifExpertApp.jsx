import { useState } from "react"
import { AddCategory, GifGrid } from "./components";

export const GifExpertApp = () => {
    const [ categories, setCategories ] = useState([]);

    const onAddCategory = ( newCategory ) => {
        if( categories.includes(newCategory) ) return;
        setCategories([ newCategory, ...categories ]);
    };

    const onDeleteCategory = ( cat) => {
        setCategories(categories.filter((category) => cat !== category))
    }

    const onReset = () => {
        setCategories([]);
    }

    return (
        <>
            <h1>GifExpertApp</h1>
            <AddCategory 
                //setCategories = { setCategories }
                onNewCategory = { onAddCategory }
            />
            
            <button onClick={onReset}>Reset</button>

            { categories.map(( category ) => (
                <GifGrid 
                    key = { category } 
                    category = { category }
                    deleteCategory = { onDeleteCategory }
                />
            ))}
            
        </>
    );
}

